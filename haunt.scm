;;; Guile website --- GNU's extension language website
;;; Copyright © 2016, 2018, 2022 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

;; This is a build file for Haunt.  Run 'haunt build' to build the web site,
;; and 'haunt serve' to serve it locally.

(use-modules (haunt site)
             (haunt reader)
	     (haunt reader commonmark)
             (haunt page)
             (haunt html)
             (haunt utils)
             (haunt builder assets)
             (haunt builder blog)
             (haunt builder atom)
             (ice-9 match)
             (srfi srfi-1)
             (website utils)
             (website types)
             (website apps base news))

(define %local-test?
  ;; True when we're testing locally, as opposed to producing things to
  ;; install to gnu.org.
  (or (getenv "GUILE_WEB_SITE_LOCAL")
      (member "serve" (command-line))))           ;'haunt serve' command

(when %local-test?
  ;; The URLs produced in these pages are only meant for local consumption.
  (format #t "~%Producing Web pages for local tests *only*!~%~%")
  (current-url-root ""))

(define (load-resources)
  "Return the list of <web-resource> objects for the web site, loaded
lazily."
  ;; Resolve the module lazily so that the 'current-url-root' change above
  ;; has the desired effect.
  (let ((module (resolve-interface '(website resources))))
    (module-ref module 'resources)))

(site #:title "GNU's programming and extension language"
      #:domain "www.gnu.org"
      #:default-metadata
      '((author . "GNU Guile Contributors")
        (email  . "guile-devel@gnu.org"))
      #:readers (list commonmark-reader sxml-reader html-reader)
      #:builders
      (cons* (static-directory "website/static" "static")
             (blog #:theme %news-haunt-theme
                   #:prefix "news")

             ;; Apparently the <link> tags of Atom entries must be absolute URLs,
             ;; hence this #:blog-prefix.
             (atom-feed #:file-name "news/feed.xml"
                        #:blog-prefix "/software/guile/news")

             (map (lambda (resource)
                    (let ((file (web-resource-path resource))
                          (body (web-resource-body resource)))
                      (lambda (site posts)
                        (make-page file
                                   (if (procedure? body)
                                       (body site posts)
                                       body)
                                   sxml->html))))
                  (load-resources))))
