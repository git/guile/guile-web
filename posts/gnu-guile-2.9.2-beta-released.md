title: GNU Guile 2.9.2 (beta) released
date: 2019-05-23 23:00
author: Andy Wingo
---
We are delighted to announce GNU Guile 2.9.2, the second beta release in
preparation for the upcoming 3.0 stable series.  See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2019-05/msg00030.html)
for full details and a download link.

This release extends just-in-time (JIT) native code generation support
to the ia32, ARMv7, and AArch64 architectures.  Under the hood, we
swapped out GNU Lightning for a related fork called
[Lightening](https://gitlab.com/wingo/lightening/), which was better
adapted to Guile's needs.

GNU Guile 2.9.2 is a beta release, and as such offers no API or ABI
stability guarantees.  Users needing a stable Guile are advised to stay
on the stable 2.2 series.

Users on the architectures that just gained JIT support are especially
encouraged to report experiences (good or bad) to `guile-devel@gnu.org`.
If you know you found a bug, please do send a note to
`bug-guile@gnu.org`.  Happy hacking!
