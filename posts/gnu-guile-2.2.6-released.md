title: GNU Guile 2.2.6 released
date: 2019-06-30 23:55
author: Ludovic Courtès
---
We are pleased to announce GNU Guile 2.2.6, the sixth bug-fix
release in the new 2.2 stable release series.  This release represents
11 commits by 4 people since version 2.2.5.  First and foremost, it
fixes a [regression](https://issues.guix.gnu.org/issue/36350) introduced
in 2.2.5 that would break Guile’s built-in HTTP server.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2019-06/msg00059.html)
for details.
