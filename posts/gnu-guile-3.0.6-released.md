title: GNU Guile 3.0.6 released
date: 2021-04-28 09:20
author: Andy Wingo
---

We are pleased to announce the release of GNU Guile 3.0.6.  This release
improves source-location information for compiled code, removes the
dependency on `libltdl`, fixes some important bugs, adds an optional
bundled "mini-gmp" library, as well as the usual set of minor
optimizations and bug fixes.  For full details, see the
[NEWS](https://git.savannah.gnu.org/cgit/guile.git/tree/NEWS?h=v3.0.6#n8)
entry.  See the [release
note](https://lists.gnu.org/archive/html/guile-devel/2021-04/msg00037.html)
for signatures, download links, and all the rest.  Happy hacking!
