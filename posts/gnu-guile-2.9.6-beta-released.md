title: GNU Guile 2.9.6 (beta) released
date: 2019-12-06 14:15
author: Andy Wingo
---
We are delighted to announce GNU Guile 2.9.6, the sixth beta release in
preparation for the upcoming 3.0 stable series.  See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2019-12/msg00006.html)
for full details and a download link.

This release fixes bugs caught by users of the previous 2.9.5
prerelease, and adds some optimizations as well as a `guile-3` feature
for `cond-expand`.

In this release, we also took the opportunity to do some more rigorous
benchmarking:

![Comparison of microbenchmark performance for Guile 2.2.6 and 2.9.6](https://wingolog.org/pub/guile-2.2.6-vs-2.9.6-microbenchmarks.png)

GNU Guile 2.9.6 is a beta release, and as such offers no API or ABI
stability guarantees.  Users needing a stable Guile are advised to stay
on the stable 2.2 series.

As always, experience reports with GNU Guile 2.9.6, good or bad, are
very welcome; send them to `guile-devel@gnu.org`.  If you know you found
a bug, please do send a note to `bug-guile@gnu.org`.  Happy hacking!
