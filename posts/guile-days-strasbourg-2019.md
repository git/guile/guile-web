title: Join the Guile and Guix Days in Strasbourg, June 21–22!
date: 2019-05-28 11:11
author: Ludovic Courtès
slug: join-guile-guix-days-strasbourg-2019
---
We’re organizing Guile Days at the University of Strasbourg, France,
[co-located with the Perl Workshop](https://journeesperl.fr/jp2019/),
on June 21st and 22nd.

![Guile Days 2019](https://www.gnu.org/software/guile/static/base/img/guile-days-2019.png)

_*Update*: The program is now complete, [view the schedule
on-line](https://journeesperl.fr/jp2019/schedule?day=2019-06-21)._

The schedule is not complete yet, but we can already announce a couple
of events:

  - _Getting Started with GNU Guix_ will be an introductory hands-on
    session to [Guix](https://guix.gnu.org/), targeting an
    audience of people who have some experience with GNU/Linux but are
    new to Guix.
  - During a “_code buddy_” session, experienced Guile programmers will
    be here to get you started programming in Guile, and to answer
    questions and provide guidance while you hack at your pace on the
    project of your choice.

If you’re already a Guile or Guix user or developer, consider submitting
by June 8th, [on the web site](https://journeesperl.fr/jp2019/newtalk),
talks on topics such as:

  - The neat Guile- or Guix-related project you’ve been working on.

  - Cool Guile hacking topics—Web development, databases, system
    development, graphical user interfaces, shells, you name it!

  - Fancy Guile technology—concurrent programming with Fibers, crazy
    macrology, compiler front-ends, JIT compilation and Guile 3,
    development environments, etc.

  - Guixy things: on Guix subsystems, services, the Shepherd, Guile
    development with Guix, all things OS-level in Guile, Cuirass,
    reproducible builds, bootstrapping, Mes and Gash, all this!

You can also propose hands-on workshops, which could last anything from
an hour to a day.  We expect newcomers at this event, people who don’t
know Guile and Guix and want to learn about it.  Consider submitting
introductory workshops on Guile and Guix!

We encourage submissions from people in communities usually
underrepresented in free software, including women, people in sexual
minorities, or people with disabilities.

We want to make this event a pleasant experience for everyone, and
participation is subject to a [code of
conduct](https://journeesperl.fr/jp2019/conduct.html).

Many thanks to the organizers of the [Perl
Workshop](https://journeesperl.fr/jp2019/) and to the sponsors of the
event: RENATER, Université de Strasbourg, X/Stra, and Worteks.
