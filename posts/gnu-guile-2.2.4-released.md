title: GNU Guile 2.2.4 released
date: 2018-07-02 11:00
author: Ludovic Courtès
---
We are delighted to announce GNU Guile 2.2.4, the fourth bug-fix
release in the new 2.2 stable release series.  It fixes many bugs that
had accumulated over the last few months, in particular bugs that could
lead to crashes of multi-threaded Scheme programs.  This release also
brings documentation improvements, the addition of
[SRFI-71](https://srfi.schemers.org/srfi-71/srfi-71.html), and better
GDB support.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2018-07/msg00006.html)
for full details and a download link.  Enjoy!
