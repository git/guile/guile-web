title: GNU Guile 2.2.5 released
date: 2019-06-20 13:20
author: Ludovic Courtès
---
We are pleased to announce GNU Guile 2.2.5, the fifth bug-fix
release in the new 2.2 stable release series.  This release represents
100 commits by 11 people since version 2.2.4.  It fixes bugs that
had accumulated over the last few months, notably in the SRFI-19 date
and time library and in the `(web uri)` module.  This release also
greatly improves performance of bidirectional pipes, and introduces the
new `get-bytevector-some!` binary input primitive that made it possible.

Guile 2.2.5 can be downloaded from [the usual
places](https://www.gnu.org/software/guile/download).

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2019-06/msg00045.html)
for details.

Besides, we remind you that Guile 3.0 is in the works, and that you can
try out [version 2.9.2, which is the latest beta release of what will
become
3.0](https://www.gnu.org/software/guile/news/gnu-guile-292-beta-released.html).

Enjoy!
