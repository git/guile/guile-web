title: GNU Guile 3.0.2 released
date: 2020-03-27 16:40
author: Ludovic Courtès
---

We are pleased to announce GNU Guile 3.0.2, the second bug-fix release of
[the new 3.0 stable
series](https://www.gnu.org/software/guile/news/gnu-guile-300-released.html)!
This release represents 22 commits by 8 people since version 3.0.1.

Among other things, this release fixes a heap corruption bug [that could
lead to random crashes](https://issues.guix.gnu.org/issue/39266) and a
rare [garbage collection issue in multi-threaded
programs](https://issues.guix.gnu.org/issue/28211).

It also adds a new module implementing [SRFI-171
_transducers_](https://srfi.schemers.org/srfi-171/srfi-171.html).

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2020-03/msg00046.html)
for details and the [download
page](https://www.gnu.org/software/guile/download/) to give it a go!
