title: GNU Guile 3.0.5 released
date: 2021-01-07 13:50
author: Andy Wingo
---

We are delighted to announce the release of GNU Guile 3.0.5.  This
release adds optimizations that can turn chains of repeated comparisons,
such as those produced by the `case` and (sometimes) the `match` macros,
into efficient O(1) table dispatches.  For full details, see the
[NEWS](https://git.savannah.gnu.org/cgit/guile.git/tree/NEWS?h=v3.0.5#n8)
entry.  See the [release
note](https://lists.gnu.org/archive/html/guile-devel/2021-01/msg00001.html)
for signatures, download links, and all the rest.  Happy hacking!
