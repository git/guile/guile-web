title: GNU Guile 2.9.4 (beta) released
date: 2019-08-25 22:25
author: Andy Wingo
---
We are delighted to announce GNU Guile 2.9.4, the fourth beta release in
preparation for the upcoming 3.0 stable series.  See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2019-08/msg00016.html)
for full details and a download link.

This release enables inlining of references to top-level definitions
within a compilation unit, speeding up some programs by impressive
amounts.  It also improves compilation of floating-point routines like
`sin`, implements the Ghuloum/Dybvig "Fixing Letrec (reloaded)"
algorithm, and allows mixed definitions and expressions within lexical
contours, as is the case at the top level.  Try it out, it's good times!

GNU Guile 2.9.4 is a beta release, and as such offers no API or ABI
stability guarantees.  Users needing a stable Guile are advised to stay
on the stable 2.2 series.

Experience reports with GNU Guile 2.9.4, good or bad, are very welcome;
send them to `guile-devel@gnu.org`.  If you know you found a bug, please
do send a note to `bug-guile@gnu.org`.  Happy hacking!
