title: GNU Guile 2.9.9 (beta) released
date: 2020-01-13 09:44
author: Andy Wingo
---

We are delighted to announce the release of GNU Guile 2.9.9.  This is
the ninth and final pre-release of what will eventually become the 3.0
release series.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2020-01/msg00047.html)
for full details and a download link.

This release fixes a number of bugs, omissions, and regressions.
Notably, it fixes the build on 32-bit systems.

We plan to release a final Guile 3.0.0 on 17 January: this Friday!
Please do test this prerelease; build reports, good or bad, are very
welcome; send them to `guile-devel@gnu.org`.  If you know you found a
bug, please do send a note to `bug-guile@gnu.org`.  Happy hacking!
