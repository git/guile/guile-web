title: GNU Guile 2.9.1 (beta) released
date: 2018-10-10 11:23
author: Andy Wingo
---
We are delighted to announce GNU Guile 2.9.1, the first beta release in
preparation for the upcoming 3.0 stable series.

This release adds support for just-in-time (JIT) native code generation,
speeding up all Guile programs.  Currently support is limited to x86-64
platforms, but will expand to all architectures supported by [GNU
Lightning](https://www.gnu.org/software/lightning/).

GNU Guile 2.9.1 is a beta release, and as such offers no API or ABI
stability guarantees.  Users needing a stable Guile are advised to stay
on the stable 2.2 series.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2018-10/msg00000.html)
for full details and a download link.  Happy hacking, and please do any
bugs you might find to `bug-guile@gnu.org`.
