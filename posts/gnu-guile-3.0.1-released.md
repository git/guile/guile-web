title: GNU Guile 3.0.1 released
date: 2020-03-08 17:14
author: Ludovic Courtès
---

We are pleased to announce GNU Guile 3.0.1, the first bug-fix release of
[the new 3.0 stable
series](https://www.gnu.org/software/guile/news/gnu-guile-300-released.html)!
This release represents 45 commits by 7 people since version 3.0.0.

Among the bug fixes is a significant performance improvement for
applications making heavy use of bignums, such as the compiler.  Also
included are fixes for an embarrassing bug in the `include` directive,
for the `hash` procedure when applied to keywords and some other
objects, portability fixes, and better R7RS support.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2020-03/msg00012.html)
for details and the [download
page](https://www.gnu.org/software/guile/download/) to give it a go!
