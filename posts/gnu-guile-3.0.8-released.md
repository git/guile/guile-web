title: GNU Guile 3.0.8 released
date: 2022-02-11 8:49:00
author: Andy Wingo
---

We are delighted to announce the release of GNU Guile 3.0.8.  This
release adds support for cross-module inlining: allowing small functions
and constants defined in one module to be inlined into their uses in
other modules.  Guile 3.0.8 also fixes a number of bugs.

For full details, see the
[NEWS](https://git.savannah.gnu.org/cgit/guile.git/tree/NEWS?h=v3.0.8#n8)
entry.  See the [release
note](https://lists.gnu.org/archive/html/guile-devel/2022-02/msg00030.html)
for signatures, download links, and all the rest.  Onwards and upwards!
