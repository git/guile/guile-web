title: GNU Guile 3.0.7 released
date: 2021-05-10 11:00
author: Andy Wingo
---

We are humbled to announce the release of GNU Guile 3.0.7.  This release
fixes a number of bugs, a couple of which were introduced in the
previous release.  For full details, see the
[NEWS](https://git.savannah.gnu.org/cgit/guile.git/tree/NEWS?h=v3.0.7#n8)
entry.  See the [release
note](https://lists.gnu.org/archive/html/guile-devel/2021-05/msg00002.html)
for signatures, download links, and all the rest.  Happy hacking!
