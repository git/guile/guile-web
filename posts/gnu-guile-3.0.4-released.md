title: GNU Guile 3.0.4 released
date: 2020-06-24 17:00
author: Ludovic Courtès
---

We are pleased but also embarrassed to announce GNU Guile 3.0.4.  This
release fixes the `SONAME` of `libguile-3.0.so`, which was wrongfully
bumped in
[3.0.3](https://www.gnu.org/software/guile/news/gnu-guile-303-released.html).
Distributions should use 3.0.4.

Apologies for the inconvenience!
