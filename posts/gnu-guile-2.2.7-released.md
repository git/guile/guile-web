title: GNU Guile 2.2.7 released
date: 2020-03-07 21:00
author: Ludovic Courtès
---

We are pleased to announce GNU Guile 2.2.7, the seventh bug-fix release
of the “legacy” 2.2 series (the current stable series is 3.0).  This
release represents 17 commits by 5 people since version 2.2.6.  Among
the bug fixes is a significant performance improvement for applications
making heavy use of bignums, such as the compiler.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2020-03/msg00009.html)
for details.
