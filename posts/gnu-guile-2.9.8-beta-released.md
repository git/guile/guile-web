title: GNU Guile 2.9.8 (beta) released
date: 2020-01-02 14:33
author: Andy Wingo
---

We are delighted to announce the release of GNU Guile 2.9.8.  This is
the eighth and possibly final pre-release of what will eventually become
the 3.0 release series.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2020-01/msg00000.html)
for full details and a download link.

This release fixes an error in libguile that could cause Guile to crash
in some particular conditions, and was notably experienced by users compiling Guile itself on Ubuntu 18.04.

We plan to release a final Guile 3.0.0 on 17 January, though we may
require another prerelease in the meantime.  However until then, note
that GNU Guile 2.9.8 is a beta release, and as such offers no API or ABI
stability guarantees.  Users needing a stable Guile are advised to stay
on the stable 2.2 series.

As always, experience reports with GNU Guile 2.9.8, good or bad, are
very welcome; send them to `guile-devel@gnu.org`.  If you know you found
a bug, please do send a note to `bug-guile@gnu.org`.  Happy hacking!
