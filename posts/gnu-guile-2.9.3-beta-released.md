title: GNU Guile 2.9.3 (beta) released
date: 2019-08-03 16:20
author: Andy Wingo
---
We are delighted to announce GNU Guile 2.9.3, the third beta release in
preparation for the upcoming 3.0 stable series.  See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2019-08/msg00001.html)
for full details and a download link.

This release improves the quality of the just-in-time (JIT) native code
generation, resulting in up to 50% performance improvements on some
workloads.  See the article ["Fibs, lies, and
benchmarks"](https://wingolog.org/archives/2019/06/26/fibs-lies-and-benchmarks)
for an in-depth discussion of some of the specific improvements.

GNU Guile 2.9.3 is a beta release, and as such offers no API or ABI
stability guarantees.  Users needing a stable Guile are advised to stay
on the stable 2.2 series.

Experience reports with GNU Guile 2.9.3, good or bad, are very welcome;
send them to `guile-devel@gnu.org`.  If you know you found a bug, please
do send a note to `bug-guile@gnu.org`.  Happy hacking!
