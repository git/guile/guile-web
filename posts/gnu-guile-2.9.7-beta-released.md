title: GNU Guile 2.9.7 (beta) released
date: 2019-12-13 14:31
author: Andy Wingo
---

We are delighted to announce GNU Guile 2.9.7, the seventh and hopefully
penultimate beta release in preparation for the upcoming 3.0 stable
series.  See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2019-12/msg00010.html)
for full details and a download link.

This release makes Guile go faster.  Compared to 2.9.6 there are some
significant improvements:

![Comparison of microbenchmark performance for Guile 2.9.6 and 2.9.7](https://wingolog.org/pub/guile-2.9.6-vs-2.9.7-microbenchmarks.png)

The cumulative comparison against 2.2 is finally looking like we have no
significant regressions:

![Comparison of microbenchmark performance for Guile 2.2.6 and 2.9.7](https://wingolog.org/pub/guile-2.2.6-vs-2.9.7-microbenchmarks.png)

Now we're on the home stretch!  Hopefully we'll get out just one more
prerelease and then release a stable Guile 3.0.0 in January.  However
until then, note that GNU Guile 2.9.7 is a beta release, and as such
offers no API or ABI stability guarantees.  Users needing a stable Guile
are advised to stay on the stable 2.2 series.

As always, experience reports with GNU Guile 2.9.7, good or bad, are
very welcome; send them to `guile-devel@gnu.org`.  If you know you found
a bug, please do send a note to `bug-guile@gnu.org`.  Happy hacking!
