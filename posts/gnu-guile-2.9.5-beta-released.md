title: GNU Guile 2.9.5 (beta) released
date: 2019-11-22 16:15
author: Andy Wingo
---
We are delighted to announce GNU Guile 2.9.5, the fifth beta release in
preparation for the upcoming 3.0 stable series.  See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2019-11/msg00016.html)
for full details and a download link.

Besides the usual set of optimizations, this release adds an `--r6rs`
option for better R6RS support out of the box, and also adds a new
`--r7rs` corresponding to R&RS.  Guile's core exception handling has
also been rebased onto the `raise-exception` and
`with-exception-handler` primitives, enabling better compatibility going
forward with structured exception objects, which are more common in the
broader Scheme community than Guile's old `throw` and `catch`.

GNU Guile 2.9.5 is a beta release, and as such offers no API or ABI
stability guarantees.  Users needing a stable Guile are advised to stay
on the stable 2.2 series.

Experience reports with GNU Guile 2.9.5, good or bad, are very welcome;
send them to `guile-devel@gnu.org`.  If you know you found a bug, please
do send a note to `bug-guile@gnu.org`.  Happy hacking!
