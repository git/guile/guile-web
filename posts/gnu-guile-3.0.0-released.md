title: GNU Guile 3.0.0 released
date: 2020-01-16 14:00
author: Andy Wingo
---

We are ecstatic and relieved to announce the release of GNU Guile 3.0.0.
This is the first release in the new stable 3.0 release series.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2020-01/msg00080.html)
for full details and a download link.

The principal new feature in Guile 3.0 is just-in-time (JIT) native code
generation.  This speeds up the performance of all programs.  Compared
to 2.2, microbenchmark performance is around twice as good on the whole,
though some individual benchmarks are up to 32 times as fast.

![Comparison of microbenchmark performance for Guile 3.0 versus 2.2](https://wingolog.org/pub/guile-3.0.0-vs-2.2.6-microbenchmarks.png)

For larger use cases, notably, this finally makes the performance of
["eval" as written in
Scheme](https://wingolog.org/archives/2009/12/09/in-which-our-protagonist-forgoes-modesty)
faster than "eval" written in C, as in the days of Guile 1.8.

Other new features in 3.0 include support for [interleaved definitions
and expressions in lexical
contexts](https://www.gnu.org/software/guile/manual/html_node/Internal-Definitions.html#Internal-Definitions),
[native support for structured
exceptions](https://www.gnu.org/software/guile/manual/html_node/Exception-Objects.html),
better support for the
[R6RS](https://www.gnu.org/software/guile/manual/html_node/R6RS-Support.html)
and
[R7RS](https://www.gnu.org/software/guile/manual/html_node/R7RS-Support.html)
Scheme standards, along with a pile of optimizations.  See the [NEWS
file](https://git.savannah.gnu.org/cgit/guile.git/tree/NEWS#n8) for a
complete list of user-visible changes.

Guile 3.0.0 and all future releases in the 3.0.x series are
[parallel-installable](https://www.gnu.org/software/guile/manual/html_node/Parallel-Installations.html#Parallel-Installations)
with other stable release series (e.g. 2.2).  As the first release in a
new stable series, we anticipate that Guile 3.0.0 might have build
problems on uncommon platforms; bug reports are very welcome.  Send any
[bug
reports](https://www.gnu.org/software/guile/manual/html_node/Reporting-Bugs.html#Reporting-Bugs)
you might have as email at to `bug-guile@gnu.org`.

Happy hacking with Guile 3!
