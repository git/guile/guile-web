title: GNU Guile 3.0.3 released
date: 2020-06-21 23:20
author: Ludovic Courtès
---

We are pleased to announce GNU Guile 3.0.3, the third bug-fix release of
[the new 3.0 stable
series](https://www.gnu.org/software/guile/news/gnu-guile-300-released.html)!
This release represents 170 commits by 17 people since version 3.0.2.

The highlight of this release is the addition of a new [_baseline
compiler_](https://wingolog.org/archives/2020/06/03/a-baseline-compiler-for-guile),
used at optimizations levels `-O1` and `-O0`.  The baseline compiler is
designed to generate code fast, for applications where compilation speed
matters more than execution time of the generated code.  It is around
ten times faster than the optimizing continuation-passing style (CPS)
compiler.

This version also includes [a new `pipeline`
procedure](https://www.gnu.org/software/guile/manual/html_node/Pipes.html#index-pipeline)
to create shell-like process pipelines, improvements to the [bitvector
interface](https://www.gnu.org/software/guile/manual/html_node/Bit-Vectors.html),
and bug fixes for JIT compilation on ARMv7 machines.

See the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2020-06/msg00008.html)
for details and the [download
page](https://www.gnu.org/software/guile/download/) to give it a go!
