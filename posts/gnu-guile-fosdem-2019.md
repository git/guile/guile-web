title: GNU Guile at FOSDEM
date: 2019-01-29 15:00
author: Ludovic Courtès
slug: gnu-guile-fosdem-2019
---

GNU Guile will be present this year again at
[FOSDEM](https://fosdem.org/2019/), which is just a few days away.  The
Guiler’s lair this time (in addition to the [Guix
Days](https://guix.gnu.org/blog/2019/meet-guix-at-fosdem-2019/)
right before FOSDEM for some of us!) will be the [minimalist languages
track](https://fosdem.org/2019/schedule/track/minimalistic_languages/).
Among the great talks this Saturday, don’t miss:

   - [Andy Wingo on the upcoming
     Guile 3](https://fosdem.org/2019/schedule/event/guile3fasterprograms/),
     featuring just-in-time compilation—we had a sneak preview with [the
     release of 2.9.1 a couple of months
     ago](https://www.gnu.org/software/guile/news/gnu-guile-291-beta-released.html)!
   - Christopher Lemmer Webber, who is well known in Guile and Guix
     circles among many other things, will [reflect on a year of Racket
     from a Guiler’s
     viewpoint](https://fosdem.org/2019/schedule/event/guileracket/).
   - Long-time Guile hacker Mike Gran [will discuss templating languages
     for interactive
     fiction](https://fosdem.org/2019/schedule/event/templatinglanguagesinteractive/)
     and contrast their design choices with those typically made in
     Scheme.
   - This track will also be home to talks about
     [GNU Mes](https://fosdem.org/2019/schedule/event/gnumes/),
     [GNU Guix](https://fosdem.org/2019/schedule/event/gnuguixminimalism/),
     and the [Guix Workflow Language
     (GWL)](https://fosdem.org/2019/schedule/event/guixinfra/), all of
     which are tightly connected to Guile.

We’re looking forward to meeting you in Brussels!
