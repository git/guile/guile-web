title: GNU Guile 3.0.9 released
date: 2023-01-25 15:30
author: Ludovic Courtès
---

We are pleased to announce the release of GNU Guile 3.0.9!  This release
fixes a number of bugs and adds several new features, among which:

  - New bindings for POSIX functionality, including bindings for the
    `at` family of functions ([`openat`, `statat`,
    etc.](https://www.gnu.org/software/guile/manual/html_node/Ports-and-File-Descriptors.html#index-openat)),
    a new
    [`spawn`](https://www.gnu.org/software/guile/manual/html_node/Processes.html#index-spawn)
    procedure that wraps
    [`posix_spawn`](https://pubs.opengroup.org/onlinepubs/9699919799/functions/posix_spawn.html)
    and that `system*` now uses, and the ability to pass flags such as
    `O_CLOEXEC` to the `pipe` procedure.
  - A new
    [`bytevector-slice`](https://www.gnu.org/software/guile/manual/html_node/Bytevector-Slices.html)
    procedure.
  - Reduced memory consumption for the linker and assembler.

For full details, see the
[NEWS](https://git.savannah.gnu.org/cgit/guile.git/tree/NEWS?h=v3.0.9#n8)
entry, and check out the [download page](/software/guile/download).

Happy Guile hacking!
