title: GNU Guile 3.0.10 released
date: 2024-06-24 15:30
author: Andy Wingo
---

We are pleased to finally announce the release of GNU Guile 3.0.10!
This release is mainly a bug-fix release, though it does include a
number of new features:

  - Better ability to define new port types in Scheme ([R6RS custom
    textual
    ports](https://www.gnu.org/software/guile/manual/html_node/Custom-Ports.html),
    [a new soft port
    interface](https://www.gnu.org/software/guile/manual/html_node/Soft-Ports.html),
    [low-level custom
    ports](https://www.gnu.org/software/guile/manual/html_node/Low_002dLevel-Custom-Ports.html)).
  - Support for local `define` definitions in all forms with bodies:
    `when` and `unless`, `cond` and `case` clauses, and so on.
  - An experimental opt-in surface syntax,
    [WISP](https://www.gnu.org/software/guile/manual/html_node/SRFI_002d119.html#index-wisp).

For full details, see the [release
announcement](https://lists.gnu.org/archive/html/guile-devel/2024-06/msg00039.html),
and check out the [download page](/software/guile/download).

Happy Guile hacking!
