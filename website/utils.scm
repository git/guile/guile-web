;;; Guile website --- GNU's extension language website
;;; Copyright © 2015, 2020, 2021, 2022 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website utils)
  #:export (current-url-root
	    latest-guile-version
	    site-url
	    static-url))

;;; VARIABLES
(define current-url-root
  ;; Website local url prefix.
  (make-parameter "/software/guile"))

(define latest-guile-version
  (make-parameter "3.0.10"))

;;; URLs
(define* (site-url #:optional (path ""))
  (string-append (current-url-root) "/" path))

(define (static-url path)
  (string-append (site-url) "static/" path))
