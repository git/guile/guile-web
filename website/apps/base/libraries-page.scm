;;; Guile website --- GNU's extension language website
;;; Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;; Copyright © 2015 Mathieu Lirzin <mthl@gnu.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base libraries-page)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (texinfo)
  #:use-module (texinfo html)
  #:use-module (website apps base components)
  #:use-module (website utils)
  #:use-module (guix licenses)
  #:use-module ((guix utils)
                #:select (version-major+minor file-sans-extension))
  #:use-module ((guix packages) #:prefix guix:)
  #:use-module (gnu packages)
  #:use-module (ice-9 match)
  #:export (libraries-page))

(define (texi->shtml str)
  "Return a SHTML representation of texinfo fragment STR."
  ;; 'texi-fragment->stexi' uses a string port so make sure it's a
  ;; Unicode-capable one (see <http://bugs.gnu.org/11197>.)
  (with-fluids ((%default-port-encoding "UTF-8"))
    (stexi->shtml (texi-fragment->stexi str))))

;;; A package entry.
(define-record-type <package>
  (make-package name description url tags license)
  package?
  (name        package-name)            ;string
  (description package-description)     ;string
  (url         package-url)             ;string
  (tags        package-tags)            ;list of strings
  (license     package-license))        ;string

(define* (package #:key name description url tags license)
  "Convenient procedure which uses keywords when defining a package entry."
  (make-package name description url tags license))

(define (package->shtml pkg)
  "Return an SHTML representation of package PKG."
  `(section
    (@ (class "lib"))
    (h3 (a (@ (href ,(package-url pkg)))
           ,(package-name pkg)))
    (p ,@(map (lambda (tag)
                `(span (@ (class "guile-version-tag")) ,tag))
              (package-tags pkg)))
    ,(texi->shtml (package-description pkg))
    (p (b "License: ") ,(package-license pkg))))

(define (guix->package pkg)
  "Return the <package> record corresponding to PKG, a Guix package."
  (define real-name
    (let ((name (guix:package-name pkg)))
      (cond ((member name '("guile-bash" "guile-gnunet" "guile-wm"))
             name)
            ((or (string-prefix? "guile-" name)
                 (string-prefix? "guile2.0-" name)
                 (string-prefix? "guile2.2-" name)
                 (string-prefix? "guile3.0-" name))
             (string-downcase
              (string-trim-right (file-sans-extension
                                  (basename (guix:package-home-page pkg)))
                                 #\/)))
            (else
             name))))

  (define tags
    ;; List of supported Guile versions.
    (match (find guile-dependency?
                 (guix:package-direct-inputs pkg))
      ((_ guile) (list (string-append "Guile "
                                      (version-major+minor
                                       (guix:package-version guile)))))
      (_ '())))

  (package #:name real-name
           #:description (guix:package-description pkg)
           #:url (guix:package-home-page pkg)
           #:license (match (guix:package-license pkg)
                       ((? license? license)
                        (license-name license))
                       ((lst ...)
                        (string-join (map license-name lst) ", "))
                       (#f ""))
           #:tags tags))

(define guile-dependency?
  ;; Return true for inputs that correspond to Guile.
  (match-lambda
    ((label (? guix:package? pkg))
     (member (guix:package-name pkg) '("guile" "guile-next")))
    (x
     #f)))

(define (package-list)
  "Return a sorted list of <package> records to display on this page."
  (define (relevant? package)
    (and (not (guix:package-superseded package))
         (not (member (guix:package-name package)
                      '("binutils" "binutils-vc4" "gdb-arm-none-eabi"
                        "guile" "guile-readline" "guile3.0-readline"
                        "guile2.0-haunt" "gdb-minimal" "gnutls-dane"
                        "ld-wrapper" "ld-gold-wrapper" "lsh"
                        "tcc-wrapper")))))

  (define lst
    ;; List of packages sans $GUIX_PACKAGE_PATH.
    (parameterize ((%package-module-path %default-package-module-path))
      (fold-packages (lambda (package result)
                       (if (and (relevant? package)
                                (any guile-dependency?
                                     (guix:package-direct-inputs package)))
                           (cons (guix->package package) result)
                           result))
                     '())))

  (define (coalesce lst)
    ;; Merge same-named packages, concatenating their tags.
    (define table
      (make-hash-table))

    (for-each (lambda (package)
                (let ((name (package-name package)))
                  (match (hash-ref table name)
                    (#f (hash-set! table name package))
                    (previous
                     (let* ((tags (package-tags package))
                            (old  (package-tags previous))
                            (new  (sort (delete-duplicates (append old tags))
                                        string>?)))
                       (hash-set! table name
                                  (set-field package (package-tags) new)))))))
              lst)
    (hash-fold (lambda (name package lst)
                 (cons package lst))
               '()
               table))

  (sort (coalesce lst)
        (lambda (p1 p2)
          (string<? (package-name p1) (package-name p2)))))


;;;
;;; Libraries page.
;;;

(define (libraries-page)
  "Return the libraries page in SXML."
  `(html
    (@ (lang "en"))
    ,(html-head "Libraries"
                #:css (static-url "base/css/libraries.css"))
    (body
     ,(site-menu #:active-link "Libraries")
     (main
      (section
       (@ (class "page-summary centered noise-bg"))
       (h1 "Libraries")
       (p "This page lists free software projects that use or enhance the
current stable version of Guile.  It is generated from the package collection
of "
          (a (@ (href "https://guix.gnu.org/")) "GNU Guix")
          ".  If you want to add packages to this list, consider "
          (a (@ (href
                 "https://guix.gnu.org/manual/en/html_node/Contributing.html"))
             "contributing them to Guix") "!"))
      (div
       (@ (class "relative"))
       (section
        (@ (class "sheet"))
        ,@(map package->shtml (package-list)))))
     ,(site-footer))))
