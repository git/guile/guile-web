;;; Guile website --- GNU's extension language website
;;; Copyright © 2015, 2021 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base components)
  #:use-module (website utils)
  #:export (html-head)
  #:export (site-menu)
  #:export (site-footer))

(define* (html-head title #:key css)
  `(head
    (title ,(string-append title " — GNU Guile"))
    (meta (@ (charset "UTF-8")))
    (meta (@ (name "author") (content "GNU Guile contributors")))
    (meta (@ (name "keywords") (content "GNU, Guile, Scheme, Computing, Computer Science, Free Software, Libre Software, Free Cultural Works, Games, Programming, Programming language, Extension language, Scheme")))
    (meta (@ (name "description") (content " Guile is the GNU Ubiquitous Intelligent Language for Extensions, the official extension language for the GNU operating system.")))
    (meta (@ (name "viewport") (content "width=device-width, initial-scale=1.0")))    
    (link (@ (rel "stylesheet") (href ,(static-url "base/css/base.css"))))
    ,(if css `(link (@ (rel "stylesheet") (href ,css))))
    (link (@ (rel "icon") (type "image/png")
	     (href ,(static-url "base/img/favicon.png"))))))


(define* (site-menu #:key (active-link "About"))
  "Return an SXML representation of the Web site's main menu bar."
  `(header
    (@ (id "site-menu"))
    (h1
     (a (@ (id "branding") (href ,(site-url)))
	(span "GNU Guile")))
    (nav
     (@ (id "pages-box"))
     (h2 "Menu: ")
     (a (@ (class ,(get-page-link-class "Download" active-link))
	   (href ,(site-url "download/"))) "Download" " ")
     (a (@ (class ,(get-page-link-class "Learn" active-link))
	   (href ,(site-url "learn/"))) "Learn" " ")
     (a (@ (class ,(get-page-link-class "Libraries" active-link))
	   (href ,(site-url "libraries/"))) "Libraries" " ")
     (a (@ (class ,(get-page-link-class "Contribute" active-link))
	   (href ,(site-url "contribute/"))) "Contribute" " ")
     (a (@ (class ,(get-page-link-class "About" active-link))
	   (href ,(site-url))) "About"))))

(define (get-page-link-class link-name active-link)
  "Return the appropriate class for LINK-NAME depending on ACTIVE-LINK."
  (if (string=? (string-downcase link-name) (string-downcase active-link))
      "page-link page-link-active"
      "page-link"))

(define (site-footer)
  `(footer
    (@ (class "site-footer noise-bg"))
    (div
     (h2 "About this website")
     (p "This website is powered by GNU Guile and the "
	(a (@ (href "http://git.savannah.gnu.org/cgit/guile/guile-web.git"))
	   "source code") " is under the "
	(a (@ (href "http://www.gnu.org/licenses/agpl-3.0.html")) "GNU AGPL") ".")
     (p "Please use the "
	(a (@ (href "https://lists.gnu.org/mailman/listinfo/guile-user/")) "mailing list")
	" or the " (a (@ (href "https://kiwiirc.com/nextclient/irc.libera.chat/?nick=guile-guest#guile")) "#guile")
	" channel on the Libera IRC network for more information about GNU Guile and this "
	"website."))
    (section
     (h3 "Guile")
     (ul
      (li (a (@ (href ,(site-url))) "Home"))
      (li (a (@ (href ,(site-url "download/"))) "Download"))
      (li (a (@ (href ,(site-url "learn/"))) "Learn"))
      (li (a (@ (href ,(site-url "libraries/"))) "Libraries"))
      (li (a (@ (href "https://savannah.gnu.org/news/?group=guile")) "News"))
      (li (a (@ (href "https://kiwiirc.com/nextclient/irc.libera.chat/?nick=guile-guest#guile")) "Community"))
      (li (a (@ (href ,(site-url "contribute/"))) "Contribute"))))
    (section
     (h3 "Learn")
     (ul
      (li (a (@ (href ,(site-url "learn/#tutorials"))) "Tutorials"))
      (li (a (@ (href ,(site-url "learn/#manuals"))) "Reference manuals"))
      (li (a (@ (href ,(site-url "learn/#scheme-resources"))) "Scheme resources"))
      (li (a (@ (href ,(site-url "learn/#bibliography"))) "Suggested bibliography"))))
    (section
     (h3 "Libraries")
     (ul
      (li (a (@ (href ,(site-url "libraries/#core"))) "Core"))
      (li (a (@ (href ,(site-url "libraries/#gui"))) "GUI"))
      (li (a (@ (href ,(site-url "libraries/#file-formats"))) "File formats"))
      (li (a (@ (href ,(site-url "libraries/#networking"))) "Networking"))
      (li (a (@ (href ,(site-url "libraries/#tools"))) "Tools"))
      (li (a (@ (href ,(site-url "libraries/#apps"))) "Applications"))))
    (section
     (h3 "Contribute")
     (ul
      (li (a (@ (href ,(site-url "contribute/"))) "Project summary"))
      (li (a (@ (href ,(site-url "contribute/#bugs"))) "Report bugs"))
      (li (a (@ (href "https://savannah.gnu.org/git/?group=guile"))
	     "Source code"))))))
