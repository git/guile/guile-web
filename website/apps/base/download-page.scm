;;; Guile website --- GNU's extension language website
;;; Copyright © 2015, 2020 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base download-page)
  #:use-module (website apps base components)
  #:use-module (website utils)
  #:use-module (web client)
  #:use-module (web response)
  #:export (download-page))


(define (file-size url)
  "Return the size of the file at URL or #f if it could not be determined."
  (false-if-exception
   (response-content-length (http-head url))))

(define (tarball-url version extension)
  (string-append "https://ftp.gnu.org/gnu/guile/guile-"
                 version "." extension))

(define (signature-url version extension)
  (string-append (tarball-url version extension) ".sig"))

(define (download-links version)
  "Return the list of download links, as SXML, for VERSION."
  `(ul
    ,(map (lambda (extension)
            (define url
              (tarball-url version extension))

            `(li
              (a (@ (href ,url)) ,(basename url))
              ,(let ((size (file-size url)))
                 (if (and size (> size 0))
                     (format #f " (~,1h MB)" (/ size 1e6))
                     " "))
              ", "
              (a (@ (href ,(signature-url version extension)))
                 "signature")))
          '("tar.gz" "tar.xz" "tar.lz"))))

(define (download-page)
  "Return the download page in SXML."
  `(html
    (@ (lang "en"))
    ,(html-head "Download"
		#:css (static-url "base/css/download.css"))
    (body
     ,(site-menu #:active-link "Download")
     (main
      (section
       (@ (class "page-summary centered noise-bg"))
       (h1 ,(string-append "Download Guile " (latest-guile-version)))
       (p "Try installing Guile from your "
	  (a (@ (href "#packages")) "system packages") " first. It is likely "
	  "that Guile is already packaged for the operating system you are "
	  "using. This is the easiest and the recommended way of installing "
	  "Guile. You can also install from the "
	  (a (@ (href "#releases")) "release tarballs") ", or cloning "
	  "Guile's source code from the "
	  (a (@ (href "#repository")) "Git repository") "."))
      (div
       (@ (class "relative"))
       (nav
	(@ (class "toc"))
	(h2 "Contents")
	(ul
	 (li
	  (a (@ (href "#packages")) "Installing packaged Guile")
	  (ul
	   (li (a (@ (href "#guix")) "Guix"))
	   (li (a (@ (href "#debian")) "Debian"))
	   (li (a (@ (href "#parabola")) "Parabola"))))
	 (li (a (@ (href "#releases")) "Releases"))
	 (li (a (@ (href "#repository")) "Repository"))
         #;
	 (li (a (@ (href "#snapshots")) "Snapshots"))))
       (section
	(@ (class "sheet"))
	(h2 (@ (id "packages")) "Installing packaged Guile")
	(h3 (@ (id "guix")) "Guix")
	(P "If you use "
	   (a (@ (href "https://guix.gnu.org")) "GNU Guix")
	   ", run the "
	   "following command:")
	(pre
	 (@ (class "shell"))
	 "guix install guile")
        (p "To install a particular version, add a version suffix like "
           (tt "@3.0") ", as appropriate.")
	(h3 (@ (id "debian")) "Trisquel, Debian, etc.")
	(P "If you use Debian, " (a (@ (href "http://trisquel.info/")) "Trisquel")
           ", "
	   " or other Debian derivatives, run the following command:")
	(pre
	 (@ (class "shell"))
	 "apt-get install guile-3.0")
        (h3 (@ (id "parabola")) "Parabola")
	(P "If you use " (a (@ (href "http://www.parabola.nu/")) "Parabola")
           ", run the following command:")
	(pre
	 (@ (class "shell"))
	 "pacman -S guile")
	(h2 (@ (id "releases")) "Releases")
	(P "Releases of Guile are available as source code "
           "tarballs at "
           (a (@ (href "https://ftp.gnu.org/gnu/guile/"))
	      "https://ftp.gnu.org/gnu/guile/")
           " and on its "
           (a (@ (href "https://www.gnu.org/prep/ftp.html")) "mirrors")
           ".")
	(p "Stable and maintenance releases of Guile have even minor version "
	   "numbers:")
	(ul
	 (li "The latest release of Guile's 3.0.x series is "
             ,(latest-guile-version) ":"
             ,(download-links (latest-guile-version)))
	 (li "The last release of Guile's legacy 2.2.x series is 2.2.7:"
             ,(download-links "2.2.7")))
        #;
	(p "Development releases of Guile are given odd minor version "
	   "numbers, and can be downloaded from "
	   (a (@ (href "http://alpha.gnu.org/gnu/guile/"))
	      "http://alpha.gnu.org/gnu/guile/") ".")

	(h2 (@ (id "repository")) "Repository")
	(P "Guile's source code is stored in a "
	   (a (@ (href "http://git-scm.com/")) "Git") " repository "
	   "and can be fetched with the following command: ")
	(pre
	 (@ (class "shell"))
	 "git clone https://git.sv.gnu.org/git/guile.git")

	(p "Developers with a "
	   (a (@ (href "http://savannah.gnu.org/")) "Savannah") " account "
	   "can access the repository over SSH:")
	(pre
	 (@ (class "shell"))
	 "git clone ssh://git.sv.gnu.org/srv/git/guile.git")
	(p "The repository can also be "
	   (a (@ (href "http://git.sv.gnu.org/gitweb/?p=guile.git"))
	      "browsed on-line") ".")
        #;
	(h2 (@ (id "snapshots")) "Snapshots")
        #;
	(P " Snapshots are available from our continuous integration system: ")
        #;
	(ul
	 (li (a (@ (href "http://hydra.nixos.org/job/gnu/guile-2-2/tarball/latest")) "2.2 stable series"))
	 (li (a (@ (href "http://hydra.nixos.org/job/gnu/guile-2-0/tarball/latest")) "2.0 old stable series"))
	 (li (a (@ (href "http://hydra.nixos.org/job/gnu/guile-master/tarball/latest")) "development, " (code "master") " branch"))))))
     ,(site-footer))))
