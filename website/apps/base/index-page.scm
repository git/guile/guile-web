;;; Guile website --- GNU's extension language website
;;; Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base index-page)
  #:use-module (srfi srfi-1)
  #:use-module (website apps base components)
  #:use-module (website apps base news)
  #:use-module (website utils)
  #:use-module (haunt post)
  #:export (index-page))


(define (manual-ref page title)
  "Return a reference to the given PAGE of the manual, with the given TITLE."
  `(a (@ (href ,(site-url (string-append "manual/html_node/" page))))
      ,title))

(define (index-page site posts)
  "Return the index page in SXML."
  `(html (@ (lang "en"))
	 ,(html-head "GNU's programming and extension language"
		     #:css (static-url "base/css/index.css"))
	 (body
	  ,(site-menu #:active-link "About")
	  (main
	   (section
	    (@ (id "featured-box") (class "info-box text-left"))
	    (div
	     (h2 "Guile is a programming language")
	     (img (@ (class "responsive-image centered-block")
		     (src ,(static-url "base/img/kid-programming-a-robot.png"))
		     (alt "")))
	     (p "Guile is designed to help programmers create flexible "
		"applications that can be extended by users or other "
		"programmers with plug-ins, modules, or scripts.")
	     (p "With Guile you can create applications and games for "
		"the " (a (@ (href "#apps-using-guile")) "desktop") ", "
		"the " (a (@ (href "#apps-using-guile")) "Web") ", "
		"the " (a (@ (href "#apps-using-guile")) "command-line")
		", and more.")
	     (p (@ (class "button-box"))
		(a (@ (class "red-button") (href ,(site-url "download/")))
		   "Download") " "
		(a (@ (class "red-outline-button") (href ,(site-url "learn/")))
		   "Get started"))))
	   (section
	    (@ (id "code-examples") (class "free-flow-box gears-bg"))
	    (h2 "Code examples")
	    (p "Guile is an implementation of the "
	       (a (@ (href "http://schemers.org/")) "Scheme")
	       " programming language, supporting the "
	       (a (@ (href "http://www.schemers.org/Documents/Standards/R5RS/"))
		  "Revised" (sup "5"))
	       " and most of the "
	       (a (@ (href "http://www.r6rs.org/"))
		  "Revised" (sup "6"))
	       " language reports, as well as many "
	       (a (@ (href "http://srfi.schemers.org/")) "SRFIs")
	       ". It also comes with a library of modules that offer "
	       "additional features, like an HTTP server and client, XML "
	       "parsing, and object-oriented programming.")
	    ;; TODO: Read code examples from files in "static/base/code".
	    (section
	     (@ (class "code-example"))
	     (pre
	      (@ (class "code"))
	      (span (@ (class "comment single")) ";;; Hello world program") "\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "keyword")) "define ")
	      (span (@ (class "name variable")) "name") " "
	      (span (@ (class "literal string")) "\"World\"")
	      (span (@ (class "punctuation")) ")") "\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name builtin")) "display ")
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name builtin")) "string-append ")
	      (span (@ (class "literal string")) "\"Hello \"") " "
	      (span (@ (class "name variable")) "name") " "
	      (span (@ (class "literal string")) "\"!\"")
	      (span (@ (class "punctuation")) "))") "\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "newline")
	      (span (@ (class "punctuation")) ")"))
	     (h3 "Hello world program")
	     (p ,(manual-ref "Definition.html" "Define a variable")
                " called " (code "name") ", join the texts "
		(code "Hello") ", " (code name) ", and " (code "!")
		" together to form the greeting " (code "Hello world!")
		", and display it on the screen."))
	    (section
	     (@ (class "code-example"))
	     (pre
	      (@ (class "code"))
	      (span (@ (class "comment single"))
		    ";;; Show current date and time") "\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "use-modules") " "
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "srfi") " "
	      (span (@ (class "name variable")) "srfi-19")
	      (span (@ (class "punctuation")) "))") "\n\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name builtin")) "display ")
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "date->string") " "
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "current-date")
	      (span (@ (class "punctuation")) ")") "\n                       "
	      (span (@ (class "literal string")) "\"~A, ~B ~e ~Y ~H:~S\"")
	      (span (@ (class "punctuation")) "))") "\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "newline")
	      (span (@ (class "punctuation")) ")"))
	     (h3 "Importing modules")
	     (p ,(manual-ref "Modules.html" "Import")
                " the "
                ,(manual-ref "SRFI_002d19.html" "srfi-19 module")
                " and use its functions to display "
		"the current date and time in the format "
		"WEEKDAY, MONTH MONTHDAY YEAR HOUR:SECOND."))
	    (section
	     (@ (class "code-example"))
	     (pre
	      (@ (class "code"))
	      (span (@ (class "comment single")) ";;; Hello HTTP server") "\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "use-modules") " "
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "web") " "
	      (span (@ (class "name variable")) "server")
	      (span (@ (class "punctuation")) "))") "\n\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "keyword")) "define ")
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "my-handler") " "
	      (span (@ (class "name variable")) "request") " "
	      (span (@ (class "name variable")) "request-body")
	      (span (@ (class "punctuation")) ")") "\n  "
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name builtin")) "values ")
	      (span (@ (class "operator")) "'")
	      (span (@ (class "punctuation")) "((")
	      (span (@ (class "name function")) "content-type") " "
	      (span (@ (class "operator")) ".") " "
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "text/plain")
	      (span (@ (class "punctuation")) ")))") "\n          "
	      (span (@ (class "literal string")) "\"Hello World!\"")
	      (span (@ (class "punctuation")) "))") "\n\n"
	      (span (@ (class "punctuation")) "(")
	      (span (@ (class "name function")) "run-server") " "
	      (span (@ (class "name variable")) "my-handler")
	      (span (@ (class "punctuation")) ")"))
	     (h3 "Hello HTTP server")
	     (p "Run a "
                ,(manual-ref "Web-Server.html" "Web server")
                " that will return a response with the text "
		"\"Hello World!\" to every request sent by HTTP clients.  "
		"Open " (tt "http://localhost:8080/") " to see the result.")))
	   (section
	    (@ (class "info-box text-right"))
	    (div
	     (h2 "Guile is an extension language platform")
	     (img (@ (class "responsive-image centered-block")
		     (src ,(static-url "base/img/kid-robot-and-extension-legs.png"))
		     (alt "")))
	     (p "Guile contains an efficient compiler and virtual machine. It "
		"can be used out of the box to write programs in Scheme, or "
		"can easily be integrated with C and C++ programs.")
	     (p "Guile is the GNU Ubiquitous Intelligent Language for "
		"Extensions, and the official extension language of the "
		(a (@ (href "http://gnu.org/")) "GNU project") ".")))
	   (section
	    (@ (class "info-box text-left"))
	    (div
	     (h2 "Extend applications")
	     (img (@ (class "responsive-image centered-block")
		     (src ,(static-url "base/img/robots-and-extension-legs-1.png"))
		     (alt "")))
	     (p "In addition to Scheme, Guile includes compiler front-ends for "
		(a (@ (href "http://www.ecma-international.org/publications/standards/Ecma-262.htm")) "ECMAScript")
		" and " (a (@ (href "http://www.emacswiki.org/cgi-bin/wiki?EmacsLisp")) "Emacs Lisp")
		" (support is underway for "
		(a (@ (href "http://www.lua.org/")) "Lua") "), which means your "
		"application may be extended in the language (or languages) "
		"most appropriate for your user base. And Guile's tools for "
		"parsing and compiling are exposed as part of its standard "
		"module set, so support for additional languages can be added "
		"without writing a single line of C.")))
	   (section
	    (@ (class "info-box text-right"))
	    (div
	     (h2 "Guile empowers users with \"practical software freedom\"")
	     (img (@ (class "responsive-image centered-block")
		     (src ,(static-url "base/img/kid-flying-robots.png"))
		     (alt "")))
	     (p "Using any of the supported scripting languages, users can "
		"customize and extend applications while they are running and "
		" see the changes take place live!")
	     (p "Users can easily trade and share features by uploading and "
		"downloading the scripts, instead of trading complex patches "
		"and recompiling their applications.")
	     (p (@ (class "button-box"))
		(a (@ (class "red-outline-button") (href ,(site-url "learn/")))
		   "Read more"))))
	   (section
	    (@ (id "apps-using-guile") (class "free-flow-box noise-bg"))
	    (h2 "Applications using Guile")
	    (a
	     (@ (class "app-example")
		(href "https://guix.gnu.org/"))
	     (img (@ (src ,(static-url "base/img/apps-thumbnails/guixsd.png"))
		     (alt "")))
	     (h3 "GNU Guix")
	     (p "Package manager and GNU distribution"))
	    (a
	     (@ (class "app-example")
		(href "http://gnucash.org/"))
	     (img (@ (src ,(static-url "base/img/apps-thumbnails/gnucash.png"))
		     (alt "")))
	     (h3 "GnuCash")
	     (p "Accounting software"))
	    (a
	     (@ (class "app-example")
		(href "https://github.com/lepton-eda/"))
	     (img (@ (src ,(static-url "base/img/apps-thumbnails/geda.png"))
		     (alt "")))
	     (h3 "Lepton-EDA")
	     (p "Suite for Electronic Design Automation"))
            (a
             (@ (class "app-example")
                (href "https://www.gnu.org/software/gdb/"))
             (img (@ (src ,(static-url "base/img/apps-thumbnails/gdb.png"))
                     (alt "")))
             (h3 "GDB")
             (p "The GNU debugger")))
	   (section
	    (@ (id "news") (class "free-flow-box"))
	    (h2 "News")
	    ,@(map (lambda (post)
                     (post->summary-sxml post
                                         (post-url post site)))
                   (take (posts/reverse-chronological posts)
                         (min 3 (length posts))))
	    (p (@ (class "button-box"))
	       (a (@ (class "red-outline-button")
		     (href ,(site-url "news")))
		  "More news"))))
	  ,(site-footer))))
