;;; Guile website --- GNU's extension language website
;;; Copyright © 2015, 2021 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base contribute-page)
  #:use-module (website apps base components)
  #:use-module (website utils)
  #:export (contribute-page))


(define (contribute-page)
  "Return the contribute page in SXML."
  `(html
    (@ (lang "es"))
    ,(html-head "Contribute"
		#:css (static-url "base/css/contribute.css"))
    (body
     ,(site-menu #:active-link "Contribute")
     (main
      (section
       (@ (class "page-summary centered noise-bg"))
       (h1 "Contribute")
       (p "GNU Guile is a large project developed mostly by volunteers from "
	  "all around the world. You are welcome to join us in the "
	  (a (@ (href "http://mail.gnu.org/mailman/listinfo/guile-devel/"))
	     "development mailing list") " or the "
	  (a (@ (href "https://kiwiirc.com/nextclient/irc.libera.chat/?nick=guile-guest#guile"))
	     "#guile channel") " on the Libera IRC network. Tell us how would you "
	     "like to help, and we will do our best to guide you."))
      (div
       (@ (class "relative"))
       (nav
	(@ (class "toc"))
	(h2 "Contents")
	(ul
	 (li (a (@ (href "#pms")) "Project Management System"))
	 (li (a (@ (href "#bugs")) "Reporting bugs"))))
       (section
	(@ (class "sheet"))
	(h2 (@ (id "pms")) "Project Management System")
	(p "We use " (a (@ (href "https://savannah.gnu.org/")) "Savannah")
	   " as the central point for development, maintenance and "
	   "distribution of GNU Guile.")
	(p "The source files for all the components of the project, "
	   "including software, web site, documentation, and artwork, "
	   "are available in "
	   (a (@ (href "https://savannah.gnu.org/git/?group=guile"))
	      "Git repositories") " at Savannah.")
	(h2 (@ (id "bugs")) "Reporting bugs")
	(p "You do us a much-appreciated service when you report the bugs "
	   "you find in Guile. Please don't assume we already know about "
	   "them.")
	(p "To report a bug, send mail to "
	   (a (@ (href "mailto:bug-guile@gnu.org")) "bug-guile@gnu.org")
	   ". Before that, make sure it is not already in the "
	   (a (@ (href "http://debbugs.gnu.org/guile")) "bug database") ".")
	(p "When you report a bug, please try to provide exact directions "
	   "we can follow to make the bug show itself when we test on our "
	   "systems. If we can reproduce the bug, it's almost certain we "
	   "will be able to fix it.")
	(p "If you have fixed the bug as well, we ask that you:")
	(ul
	 (li "Please send a patch and commit log as produced by "
	     (code "git format-patch") ".")
	 (li "Please include a "
	     (a (@ (href "http://www.gnu.org/prep/standards/standards.html#Change-Logs")) "ChangeLog-style commit log")
	     " with your fix.")
	 (li "Include references to the bug report. Turn the example that "
	     "triggers the bug into a test case, submitted as part of your "
	     "patch.")
	 (li "If the patch changes more than about ten lines of code, we "
	     "may ask you to assign your copyright to the FSF before it can "
	     "be applied.")))))
     ,(site-footer))))
