;;; Guile website --- GNU's extension language website
;;; Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website types)
  #:use-module (srfi srfi-9)
  #:export (make-web-resource
	    web-resource?
	    web-resource-body
	    web-resource-path))

;;; Web resource
;;;
;;; A web resource is a representation of an HTML page, an RSS feeds or any
;;; other document that can be expressed in SXML.
;;;
;;; Fields:
;;;
;;; body (sxml)
;;;     SXML representation of the resource.
;;;
;;; path (string)
;;;     URL path to the resource. For example, "blog/index.html".
(define-record-type <web-resource>
  (make-web-resource path body)
  web-resource?
  (body web-resource-body)                        ;sxml | site posts -> sxml
  (path web-resource-path))
