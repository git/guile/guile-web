=================
Designs for Guile
=================

This directory contains SVG designs for Guile logo and website.


Copying
=======

All the graphics in this directory are available under the following terms:

    Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>

    Permission is granted to copy, distribute and/or modify this work under the
    terms of the Creative Commons Attribution-ShareAlike 4.0 International
    License
